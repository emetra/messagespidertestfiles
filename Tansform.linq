<Query Kind="Program">
  <Namespace>System.Xml.Xsl</Namespace>
  <AppConfig>
    <Content>
      <configuration>
        <configSections>
          <sectionGroup name="system.xml">
            <section name="xslt" type="System.Xml.XmlConfiguration.XsltConfigSection, System.Xml, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" requirePermission="false" />
          </sectionGroup>
        </configSections>
        <system.xml>
          <xslt limitXPathComplexity="false" />
        </system.xml>
      </configuration>
    </Content>
  </AppConfig>
</Query>

void Main()
{
	var file = @"C:\work\WIP\MessageSpider\XML\Locations\FileReadMessageLocation\Case-1a.xml";
	var stylesheet = @"C:\work\WIP\standarder\visningsfil\svarrapport\svarrapport2html.xsl";
	var xmlResolver = new XmlUrlResolver();

	var xml = XDocument.Load(file);

	XDocument newTree = new XDocument();
	using (XmlWriter writer = newTree.CreateWriter())
	{
		XmlReaderSettings xmlReaderSettings = new XmlReaderSettings();
		xmlReaderSettings.DtdProcessing = DtdProcessing.Parse;

		XsltSettings xsltSettings = new XsltSettings(true, true);
		xsltSettings.EnableDocumentFunction = true;
		xsltSettings.EnableScript = true;

		XslCompiledTransform xslt = new XslCompiledTransform();
		using (XmlReader xr = XmlReader.Create(stylesheet, xmlReaderSettings))
		{
			xslt.Load(xr,xsltSettings, xmlResolver);
		}
		//		xslt.Load(@"C:\work\WIP\standarder\visningsfil\svarrapport\svarrapport2html.xsl");
		var reader = xml.CreateReader();
		xslt.Transform(reader, writer);
	}

	newTree.Dump();
}

// Define other methods and classes here

<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="3.0" 
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
    xmlns:xs="http://www.w3.org/2001/XMLSchema" 
    xmlns:fn="http://www.w3.org/2005/xpath-functions" 
	xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" 
    xmlns:kith="http://www.kith.no/xmlstds/msghead/2006-05-24"
    xmlns:dm="http://www.kith.no/xmlstds/dialog/2006-10-11"
	xmlns="http://www.emetra.no/xmlstds/MessageSpider/Inbox">
    <xsl:output method="xml" omit-xml-declaration="yes"/>
    <xsl:template match="/">
        <xsl:apply-templates select="//kith:MsgHead"/>
    </xsl:template>
    <xsl:template match="kith:MsgHead">
        <Message>
			<xsl:attribute name="xsi:schemaLocation">http://www.emetra.no/xmlstds/MessageSpider/Inbox SpiderInbox.xsd</xsl:attribute>
            <Inbox>
                <MsgId>
                    <xsl:value-of select="kith:MsgInfo/kith:MsgId"/>
                </MsgId>
				<xsl:for-each select="kith:MsgInfo/kith:Patient/kith:Ident[kith:TypeId/@V='FNR'][1]">
					<PatientOffId>
						<xsl:value-of select="kith:Id" />
					</PatientOffId>
				</xsl:for-each>
                <PatientName>
                    <xsl:value-of select="kith:MsgInfo/kith:Patient/kith:FamilyName"/>, <xsl:value-of select="kith:MsgInfo/kith:Patient/kith:GivenName"/>
                </PatientName>
                <GenDate>
                    <xsl:value-of select="kith:MsgInfo/kith:GenDate" />
                </GenDate>
                <IssueDate>
                    <xsl:value-of select="kith:Document/kith:RefDoc/kith:IssueDate/@V" />
                </IssueDate>
                <TypeV>
                    <xsl:value-of select="kith:MsgInfo/kith:Type/@V" />
                </TypeV>
                <TypeDN>
                    <xsl:value-of select="kith:MsgInfo/kith:Type/@DN" />
                </TypeDN>
                <MsgDescrAttr>
                    <xsl:value-of select="kith:Document/kith:RefDoc/kith:Content/dm:Dialogmelding/dm:Foresporsel/dm:TypeForesp/@DN" />
                </MsgDescrAttr>
                <MsgDescrVal>
                    <xsl:value-of select="kith:Document/kith:RefDoc/kith:Content/dm:Dialogmelding/dm:Foresporsel/dm:Sporsmal" />
                </MsgDescrVal>
                <ServProviderInst>
                    <xsl:value-of select="kith:MsgInfo/kith:Sender/kith:Organisation/kith:OrganisationName" />
                </ServProviderInst>
                <ServProviderDept>
                    <xsl:value-of select="kith:MsgInfo/kith:Sender/kith:Organisation/kith:HealthcareProfessional/kith:FamilyName" /> , <xsl:value-of select="kith:MsgInfo/kith:Sender/kith:Organisation/kith:HealthcareProfessional/kith:GivenName" /> 
                </ServProviderDept>
            </Inbox>
        </Message>
    </xsl:template>
</xsl:stylesheet>






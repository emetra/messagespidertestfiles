<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet 
	version="3.0" 
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
	xmlns:xs="http://www.w3.org/2001/XMLSchema" 
	xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" 
	xmlns:fn="http://www.w3.org/2005/xpath-functions" 
	xmlns:kith="http://www.kith.no/xmlstds/labsvar/2008-12-01" 
	xmlns="http://www.emetra.no/xmlstds/MessageSpider/Inbox">
	<xsl:output method="xml" omit-xml-declaration="no"/>
	<xsl:template match="/">
		<xsl:apply-templates select="//kith:Message"/>
	</xsl:template>
	<xsl:template match="kith:Message">
		<Message>
			<xsl:attribute name="xsi:schemaLocation">http://www.emetra.no/xmlstds/MessageSpider/Inbox SpiderInbox.xsd</xsl:attribute>
			<Inbox>
				<MsgId>
					<xsl:value-of select="kith:MsgId"/>
				</MsgId>
				<PatientOffId>
					<xsl:value-of select="kith:ServReport/kith:Patient/kith:OffId"/>
				</PatientOffId>
				<PatientName>
					<xsl:value-of select="kith:ServReport/kith:Patient/kith:Name"/>
				</PatientName>
				<GenDate>
					<xsl:value-of select="kith:GenDate/@V"/>
				</GenDate>
				<IssueDate>
					<xsl:value-of select="kith:ServReport/kith:IssueDate/@V"/>
				</IssueDate>
				<TypeV>
					<xsl:value-of select="kith:Type/@V"/>
				</TypeV>
				<TypeDN>
					<xsl:value-of select="kith:Type/@DN"/>
				</TypeDN>
				<MsgDescrAttr>
					<xsl:value-of select="kith:ServReport/kith:MsgDescr/@DN"/>
				</MsgDescrAttr>
				<MsgDescrVal>
					<xsl:value-of select="kith:ServReport/kith:MsgDescr"/>
				</MsgDescrVal>
				<ServProviderInst>
					<xsl:value-of select="kith:ServReport/kith:ServProvider/kith:HCP/kith:Inst/kith:Name"/>
				</ServProviderInst>
				<ServProviderDept>
					<xsl:value-of select="kith:ServReport/kith:ServProvider/kith:HCP/kith:Inst/kith:Dept/kith:Name"/>
				</ServProviderDept>
			</Inbox>
		</Message>
	</xsl:template>
</xsl:stylesheet>

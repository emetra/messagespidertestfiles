<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet
	version="3.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:xs="http://www.w3.org/2001/XMLSchema"
	xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
	xmlns:fn="http://www.w3.org/2005/xpath-functions"
	xmlns:kith="http://www.kith.no/xmlstds/msghead/2006-05-24"
	xmlns:dlg="http://www.kith.no/xmlstds/dialog/2006-10-11"
	xmlns:henv="http://www.kith.no/xmlstds/henvisning/2005-07-08"
	xmlns="http://www.emetra.no/xmlstds/MessageSpider/Inbox">
	<xsl:output method="xml" omit-xml-declaration="no"/>
	<xsl:template match="/">
		<xsl:apply-templates select="//henv:Message"/>
	</xsl:template>
	<xsl:template match="henv:Message">
		<Message>
			<xsl:attribute name="xsi:schemaLocation">http://www.emetra.no/xmlstds/MessageSpider/Inbox SpiderInbox.xsd</xsl:attribute>
			<Inbox>
				<MsgId>
					<xsl:value-of select="henv:MsgId"/>
				</MsgId>
				<PatientOffId>
					<xsl:value-of select="henv:ServReq/henv:Patient/henv:OffId"/>
				</PatientOffId>
				<PatientName>
					<xsl:value-of select="henv:ServReq/henv:Patient/henv:Name"/>
				</PatientName>
				<GenDate>
					<xsl:value-of select="henv:GenDate/@V"/>
				</GenDate>
				<IssueDate>
					<xsl:value-of select="henv:ServReq/henv:IssueDate/@V"/>
				</IssueDate>
				<TypeV>
					<xsl:value-of select="henv:Type/@V"/>
				</TypeV>
				<TypeDN>
					<xsl:value-of select="henv:Type/@DN"/>
				</TypeDN>
				<MsgDescrAttr>
					<!-- <xsl:value-of select="kith:Document/kith:ContentDescription" /> -->
				</MsgDescrAttr>
				<MsgDescrVal>
					<!-- <xsl:value-of select="kith:Document/kith:RefDoc/kith:Content/dlg:Dialogmelding/dlg:Foresporsel/dlg:Sporsmal"/> -->
				</MsgDescrVal>
				<ServProviderInst>
					<xsl:value-of select="henv:ServReq/henv:ServProvider/henv:HCP/henv:Inst/henv:Name"/>
				</ServProviderInst>
				<ServProviderDept>
					<xsl:value-of select="henv:ServReq/henv:Requester/henv:HCP/henv:Inst/henv:Dept/henv:Name"/>
				</ServProviderDept>
			</Inbox>
		</Message>
	</xsl:template>
</xsl:stylesheet>
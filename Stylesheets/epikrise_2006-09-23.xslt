<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet
	version="3.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:xs="http://www.w3.org/2001/XMLSchema"
	xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
	xmlns:fn="http://www.w3.org/2005/xpath-functions"
	xmlns:kith="http://www.kith.no/xmlstds/msghead/2006-05-24"
	xmlns:dlg="http://www.kith.no/xmlstds/dialog/2006-10-11"
	xmlns:epikrise="http://www.kith.no/xmlstds/epikrise/2006-09-23"
	xmlns="http://www.emetra.no/xmlstds/MessageSpider/Inbox">
	<xsl:output method="xml" omit-xml-declaration="no"/>
	<xsl:template match="/">
		<xsl:apply-templates select="//epikrise:Message"/>
	</xsl:template>
	<xsl:template match="epikrise:Message">
		<Message>
			<xsl:attribute name="xsi:schemaLocation">http://www.emetra.no/xmlstds/MessageSpider/Inbox SpiderInbox.xsd</xsl:attribute>
			<Inbox>
				<MsgId>
					<xsl:value-of select="epikrise:MsgId"/>
				</MsgId>
				<PatientOffId>
					<xsl:value-of select="epikrise:ServRprt/epikrise:Patient/epikrise:OffId"/>
				</PatientOffId>
				<PatientName>
					<xsl:value-of select="epikrise:ServRprt/epikrise:Patient/epikrise:Name"/>
				</PatientName>
				<GenDate>
					<xsl:value-of select="epikrise:GenDate/@V"/>
				</GenDate>
				<IssueDate>
					<xsl:value-of select="epikrise:ServRprt/epikrise:IssueDate/@V"/>
				</IssueDate>
				<TypeV>
					<xsl:value-of select="epikrise:Type/@V"/>
				</TypeV>
				<TypeDN>
					<xsl:value-of select="epikrise:Type/@DN"/>
				</TypeDN>
				<MsgDescrAttr>
					<!-- <xsl:value-of select="kith:Document/kith:ContentDescription" /> -->
				</MsgDescrAttr>
				<MsgDescrVal>
					<!-- <xsl:value-of select="kith:Document/kith:RefDoc/kith:Content/dlg:Dialogmelding/dlg:Foresporsel/dlg:Sporsmal"/> -->
				</MsgDescrVal>
				<ServProviderInst>
					<xsl:value-of select="epikrise:ServRprt/epikrise:ServProvider/epikrise:HCP/epikrise:Inst/epikrise:Name"/>
				</ServProviderInst>
				<ServProviderDept>
					<xsl:value-of select="epikrise:ServRprt/epikrise:ServProvider/epikrise:HCP/epikrise:Inst/epikrise:Dept/epikrise:Name"/>
				</ServProviderDept>
			</Inbox>
		</Message>
	</xsl:template>
</xsl:stylesheet>